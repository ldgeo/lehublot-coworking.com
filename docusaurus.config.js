// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Le Hublot',
  tagline: 'Espaces de travail partagé en Royans-Vercors',
  url: 'https://lehublot-coworking.com',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  // organizationName: 'facebook', // Usually your GitHub org/user name.
  // projectName: 'docusaurus', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          // editUrl:
          //   'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          // editUrl:
          //   'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      metadata: [{name: 'keywords', content: "coworking, drôme, hublot, espace de travail partagé, saint jean en royans, le chalutiers, la  baume d''hostun"}],
      colorMode: {
        disableSwitch: true,
        defaultMode: "light"

      },
      navbar: {
        title: '',
        logo: {
          alt: 'Le Hublot',
          src: 'img/logo.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'stjean',
            position: 'left',
            label: 'Espace Saint-Jean',
          },
          {
            type: 'doc',
            docId: 'chalutier',
            position: 'left',
            label: "Espace Chalutier",
          },
          {
            to: '/les-hubloteurs',
            position: 'left',
            label: "Les Hubloteurs",
          },
          {
            type: 'doc',
            docId: 'tarifs',
            position: 'left',
            label: 'Nos Tarifs',
          },

          // {to: '/blog', label: 'Blog', position: 'left'},
          // {
          //   href: 'https://github.com/facebook/docusaurus',
          //   label: 'GitHub',
          //   position: 'right',
          // },
        ],
      },
      footer: {
        logo: {
          alt: 'Cédille Drôme',
          src: 'img/cedille.png',
          href: 'https://cedille.pro/',
          // width: 160,
          // height: 51,
        },
        style: 'dark',
        links: [
          {
            title: 'Lieux',
            items: [
              {
                label: 'Saint-Jean-en-Royans',
                to: 'https://www.openstreetmap.org/node/9505321717#map=16/45.0173/5.2942',
              },
              {
                label: "La Baume d'Hostun - Le Chalutier",
                to: 'https://www.openstreetmap.org/node/9505264998#map=15/45.0589/5.2222',
              }
            ],
          },
          {
            title: 'Contact',
            items: [
              {
                label: 'lehublotcoworking@gmail.com',
                href: 'mailto:lehublotcoworking@gmail.com',
              },
              {
                label: '0984240725 (St Jean)',
                href: 'tel:+33984240725',
              },
              // {
              //   label: 'Discord',
              //   href: 'https://discordapp.com/invite/docusaurus',
              // },
              // {
              //   label: 'Twitter',
              //   href: 'https://twitter.com/docusaurus',
              // },
            ],
          },
          // {
          //   title: 'More',
          //   items: [
          //     {
          //       label: 'Blog',
          //       to: '/blog',
          //     },
          //     {
          //       label: 'GitHub',
          //       href: 'https://github.com/facebook/docusaurus',
          //     },
          //   ],
          // },
        ],
        copyright: `Association Le Hublot - ${new Date().getFullYear()} `,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
