---
hide_table_of_contents: true
---

# Espace Saint-Jean

Situé au [2ter rue Fontaine Martel](https://www.openstreetmap.org/node/9505321717) à Saint Jean en Royans.

![](enseigne.jpg)

## Les bureaux & salles de réunions

![](salle.jpg)

![](salle2.jpg)

![](bureau1.jpg)

![](bureau2.jpg)
