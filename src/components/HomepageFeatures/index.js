import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Pour qui ?',
    Svg: require('@site/static/img/undraw_docusaurus_mountain.svg').default,
    description: (
      <>
        Pour des télé-travailleurs, des indépendants, créateurs et porteurs de projets qui souhaitent intégrer la dynamique
        d'un espace de travail partagé et convivial au coeur du Royans-Vercors.
      </>
    ),
  },
  {
    title: 'Deux lieux',
    Svg: require('@site/static/img/undraw_docusaurus_tree.svg').default,
    description: (
      <>
      <p/>
      Au <a href='https://lechalutier.org'>Chalutier</a> à la Baume d'Hostun au sein d'un tiers-lieu en plein développement.
      <p/>
      À Saint-Jean-en-Royans dans un bâtiment de 120m² en plein coeur du village.
      </>
    ),
  },
  {
    title: 'Une association',
    Svg: require('@site/static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        Le Hublot est un association loi 1901 dirigée par un comité d'administration
        collégial dont les membres sont des utilisateurs engagés dans la gestion des lieux.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
