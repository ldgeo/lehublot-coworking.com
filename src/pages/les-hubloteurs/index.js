import React from 'react';
import Layout from '@theme/Layout';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Unstable_Grid2';
import Container from '@mui/material/Container';
import LanguageIcon from '@mui/icons-material/Language';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import FacebookIcon from '@mui/icons-material/Facebook';
import IconButton from '@mui/material/IconButton';
import EmailIcon from '@mui/icons-material/Email';
import { createTheme, ThemeProvider, responsiveFontSizes } from '@mui/material/styles';
import trombiconfig from './_trombi.config'; // Database of all users

let theme = createTheme({
    palette: {
        primary: {
            main: '#117a77', // Define color of social buttons
        },
    },
    typography: {
        h3: {
            fontSize: '2.4rem',
        }
    },
});

theme = responsiveFontSizes(theme);


const getPhoto = function (name) { // Get the corresponding photo or display a placeholder
    try {
        return require('./photos/' + name + '.png').default
    } catch (error) {
        return require('./photos/empty.png').default
    }
};

export default function Trombi() {
    return (

        <ThemeProvider theme={theme}>
            <Layout title="Les Hubloteurs" description="Rencontrez nos adhérents">
                <Container fixed sx={{ padding: '30px' }}>
                    <Grid container spacing={3}>
                        {trombiconfig.trombinoscope
                            .sort((a, b) => a.name > b.name ? 1 : -1) // Sort by alphabetical order
                            .map((user, i) => {  // Go through all users ordered
                                return (
                                    <Grid xs={12} lg={6}>
                                        <Card sx={{ display: 'flex', flex: 'auto' }}>
                                            <CardMedia
                                                component="img"
                                                sx={{
                                                    width: {
                                                        xs: 100,
                                                        lg: 200,
                                                    },
                                                    objectFit: "contain"
                                                }}
                                                image={getPhoto(user.name)}
                                                alt={user.name}
                                                title={user.name}
                                            />
                                            <Box sx={{ display: 'flex', flexDirection: 'column', marginTop:'auto', marginBottom:'auto'}}>
                                                <CardContent>
                                                    <Typography variant="h3" color="text.primary">{user.name}</Typography>
                                                    <Typography color="text.secondary">{user.description}</Typography>
                                                </CardContent>
                                                <CardActions>
                                                    {user.website ?
                                                        <IconButton title="Site web" color="primary" size="medium" href={user.website}>
                                                            <LanguageIcon fontSize="inherit" />
                                                        </IconButton>
                                                        : ""}
                                                    {user.email ?
                                                        <IconButton title="Adresse email" color="primary" size="medium" href={"mailto:" + user.email}>
                                                            <EmailIcon fontSize="inherit" />
                                                        </IconButton>
                                                        : ""}
                                                    {user.linkedin ?
                                                        <IconButton title="Linkedin" color="primary" size="medium" href={user.linkedin}>
                                                            <LinkedInIcon fontSize="inherit" />
                                                        </IconButton>
                                                        : ""}
                                                    {user.facebook ?
                                                        <IconButton title="Facebook" color="primary" size="medium" href={user.facebook}>
                                                            <FacebookIcon fontSize="inherit" />
                                                        </IconButton>
                                                        : ""}
                                                </CardActions>
                                            </Box>
                                        </Card>
                                    </Grid>
                                )
                            })
                        }
                    </Grid>
                </Container>
            </Layout>
        </ThemeProvider>
    );
}

