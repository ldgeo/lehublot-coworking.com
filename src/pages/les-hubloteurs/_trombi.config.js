/*
Liste de tous les Hubloteurs avec les liens à afficher dans le trombinoscope.
L'ordre n'a pas d'importance, ils sont ensuite triés par ordre alphabétique.
La photo doit être placée dans le dossier "./photo/" et doit s'appeler "name".png
*/

const config = {
    trombinoscope: [
        {
            name: "Ludovic",
            description: "Artisan logiciel, Libriste",
            website: "http://cartodev.com",
            linkedin: "https://www.linkedin.com/in/ludovic-delaune-cartodev/"
        },
        {
            name: "France",
            description: "Conseillère en ressources humaines",
            website: "https://www.pastelrh.fr",
            email: "contact@pastel-rh.com",
            linkedin: "https://www.linkedin.com/company/pastel-rh",
            facebook: "https://www.facebook.com/PastelRH"

        },
        {
            name: "Bérengère",
            description: "Designer, graphiste, scénographe",
            website: "http://www.atelierba.fr"
        },

        {
            name: "Thibault",
            description: "Ingénieur informatique",
            website: "https://www.thibault-roucou.fr/",
            linkedin: "https://www.linkedin.com/in/thibault-roucou/"
        },
        {
            name: "Sarah",
            description: "Facilitatrice en intelligence collective"
        },
        {
            name: "Tangi",
            description: "Forgeur d'idées - Connaissance et protection du patrimoine naturel",
        },
        {
            name: "Elise",
            description: "Gestionnaire forestier / Expert forestier",
            email: "elise.pares@gmail.com"

        },
        {
            name: "Tiphaine",
            description: "Graphiste",
        },
        {
            name: "Alexandra",
            description: "Chargée de collecte de fonds"
        },
        {
            name: "Coralie",
            description: "Cheffe de projets bâtiments biosourcés"
        },
        {
            name: "Thomas",
            description: "Botaniste, cordiste naturaliste",
            website: "http://www.inventaire-vertical.fr",
            email: "thomasamodei@gmail.com"
        },
        {
            name: "Chloé",
            description: "Formatrice en hypnose"
        },
        {
            name: "Sabrina",
            description: "Développeuse"
        },
        {
            name: "Sidonie",
            description: "Cordinatrice, Réseau des accorderies de France"
        },
        {
            name: "Catherine",
            description: "Acheteuse"
        },
        {
            name: "Lucas",
            description: "Chef de projet numérique",
            website: "https://www.social-boulder.com",
            email: "contact@social-boulder.com"
        },
        {
            name: "Mickaël",
            description: "Cinéaste"
        },
        {
            name: "Karine",
            description: ""
        },
        {
            name: "Lucile",
            description: "Illustratrice/ intervenante en ateliers philo-art",
            website: "http://lucilelux.com"
        },
        {
            name: "Armel",
            description: "Designer politique",
            website: "http://www.democratieouverte.org",
            linkedin: "https://www.linkedin.com/in/lecoza/"
        },
        {
            name: "Sylvain",
            description: ""
        },
        {
            name: "Nils",
            description: "Cadre technique fédéral",
            website: "https://www.ffcam.fr",
            email: "n.guillotin@ffcam.fr"
        }
    ]
};
export default config;
